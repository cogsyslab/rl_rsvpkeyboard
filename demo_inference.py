import os
from modules.oracle import BinaryRSVPOracle
import time
from modules.progress_bar import progress_bar
import scipy.io as sio
from modules.main_frame import DecisionMaker, EvidenceFusion
from scipy.stats import norm, iqr
from sklearn.neighbors.kde import KernelDensity
from modules.main_frame import alphabet
from modules.language_model_folder.language_model_wrapper import LanguageModelWrapper
from modules.query_methods import NBestQuery, MomentumQuerying
import numpy as np

alp = alphabet()
len_alp = len(alp)
evidence_names = ['LM', 'Eps']

# phrase that is already typed before the task
pre_phrase = 'CA'
# target of the task
phrase = "CAE"
backspace_prob = 1. / 35.
max_num_mc = 1000
min_num_seq = 30
max_num_seq = 30

abs_path_fst = os.path.abspath("./modules/language_model_folder/fst/brown_closure.n5.kn"
                               ".fst")
lang_model = LanguageModelWrapper(abs_path_fst, alp=alp, backspace_prob=backspace_prob)

query_methods = [MomentumQuerying(alp=alp, gam=.9), NBestQuery(alp=alp)]
conjugator = EvidenceFusion(evidence_names, len_dist=len_alp)
decision_maker = DecisionMaker(state='', alp=alp)
decision_maker.min_num_seq = min_num_seq
decision_maker.max_num_seq = max_num_seq

tmp = sio.loadmat("./data/data.mat")
x = tmp['scores']
y = tmp['trialTargetness']

# data collected can have outlier samples. these samples will cause a problem once a
# KDE is fit. Therefore we discard samples which have unrealistically high score.
sc_threshold = 1000000

# modify data for outliers
y = y[x > -sc_threshold]
x = x[x > -sc_threshold]
y = y[x < sc_threshold]
x = x[x < sc_threshold]

# artificially shift mean of the positive class and separate means of the KDEs
x[y == 1] += .5

oracle = BinaryRSVPOracle(x, y, phrase=phrase, alp=alp)

# this is the dummy eeg_modelling part

# select KDE bandwidth
# ref: Silverman, B.W. (1986). Density Estimation for Statistics and Data Analysis.
# London: Chapman & Hall/CRC. p. 48. ISBN 0-412-24620-1
bandwidth = 1.06 * min(np.std(x),
                       iqr(x) / 1.34) * np.power(x.shape[0], -0.2)
classes = np.unique(y)
cls_dep_x = [x[np.where(y == classes[i])] for i in range(len(classes))]
dist = []
for i in range(len(classes)):
    dist.append(KernelDensity(bandwidth=bandwidth))

    dat = np.expand_dims(cls_dep_x[i], axis=1)
    dist[i].fit(dat)

# number of sequences spent for correct decision (adds incorrect decisions)
seq_holder = [[], []]

target_dist = [[], []]
l = max_num_mc * len(query_methods)
progress_bar(0, l, prefix='Progress:', suffix='Complete', length=50)
bar_counter = 0
method_count = 0
for query in query_methods:

    decision_maker.query_method = query
    sum_seq_till_correct = np.zeros(len(phrase))

    for idx in range(max_num_mc):
        progress_bar(bar_counter + 1, l, prefix='Progress:', suffix='Complete', length=50)

        decision_maker.reset(state=pre_phrase)
        oracle.reset()
        oracle.update_state(decision_maker.displayed_state)

        seq_till_correct = [0] * len(phrase)
        d_counter = 0
        while decision_maker.displayed_state != phrase:

            # can be used for artificial language model
            # get prior information from language model
            # tmp_displayed_state = "_" + decision_maker.displayed_state
            # idx_final_space = len(tmp_displayed_state) - \
            #                   list(tmp_displayed_state)[::-1].index("_") - 1
            # lang_model.set_reset(tmp_displayed_state[idx_final_space + 1:])
            lang_model.set_reset(decision_maker.displayed_state.replace('_', ' '))
            lm_prior = lang_model.get_prob()
            # lm_prior[alp.index(phrase)] = np.power(.1, 3)
            # lm_prior = lm_prior / np.sum(lm_prior)

            prob = conjugator.update_and_fuse({'LM': lm_prior})
            prob_new = np.array([i for i in prob])
            d, sti = decision_maker.decide(prob_new)

            while True:
                # get answers from the user
                score = oracle.answer(sti)

                # get the likelihoods for the scores
                likelihood = []
                for i in score:
                    dat = np.squeeze(i)
                    dens_0 = dist[0].score_samples(dat)[0]
                    dens_1 = dist[1].score_samples(dat)[0]
                    likelihood.append(np.asarray([dens_0, dens_1]))
                likelihood = np.array(likelihood)
                # compute likelihood ratio for the query
                lr = np.exp(likelihood[:, 1] - likelihood[:, 0])

                # initialize evidence with all ones
                evidence = np.ones(len_alp)
                c = 0
                # update evidence of the queries that are asked
                for q in sti:
                    idx = alp.index(q)
                    evidence[idx] = lr[c]
                    c += 1

                # update posterior and decide what to do
                prob = conjugator.update_and_fuse({'Eps': evidence})
                prob_new = np.array([i for i in prob])
                d, sti = decision_maker.decide(prob_new)

                # after decision update the user about the current delta
                oracle.update_state(decision_maker.displayed_state)

                # print('\r State:{}'.format(decision_maker.state)),

                if d:
                    tmp_dist = list(np.array(
                        decision_maker.list_epoch[-2]['list_distribution'])[:,
                                    alp.index(oracle.state)])
                    target_dist[method_count].append(tmp_dist)

                    break
            # Reset the conjugator before starting a new epoch for clear history
            conjugator.reset_history()
            seq_till_correct[d_counter] += len(decision_maker.list_epoch[-2]['list_sti'])
            if (decision_maker.list_epoch[-2]['decision'] == phrase[d_counter] and
                    decision_maker.displayed_state == phrase[0:len(
                        decision_maker.displayed_state)]):
                d_counter += 1

        seq_holder[method_count].append(seq_till_correct)
        bar_counter += 1
    method_count += 1

print(seq_holder)
print(np.sum(seq_holder[0]) / max_num_mc)
print(np.sum(seq_holder[1]) / max_num_mc)

mean_x = []
std_x = []
for idx in range(len(query_methods)):
    method_tar = target_dist[idx]
    tmp = np.array([method_tar[i][0:min_num_seq] for i in range(len(method_tar))])
    mean_x.append(np.mean(tmp, axis=0))
    std_x.append(.1 * np.sqrt(np.var(tmp, axis=0)))

line_space_x = np.linspace(1, mean_x[0].shape[0], mean_x[0].shape[0])

import matplotlib.pyplot as plt

list_method = ['momentum', 'n_best']
line_style_list = ['--', '-.', ':', '-', '--']
color_list = ['r', 'g', 'b', 'k', 'c']
fig = plt.figure()
ax = fig.add_subplot(221)
for idx in range(len(query_methods)):
    label = list_method[idx]
    line_style = line_style_list[idx]
    color = color_list[idx]
    ax.fill_between(line_space_x, mean_x[idx] - std_x[idx],
                    mean_x[idx] +
                    std_x[idx], facecolor=color, alpha=0.1)
    ax.plot(line_space_x, mean_x[idx], color=color,
            linestyle=line_style,
            linewidth=2, label=label)

plt.show()
